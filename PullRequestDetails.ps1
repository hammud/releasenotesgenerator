$project = "hammud"
$repository = "releasenotesgenerator"
$uri = "https://api.bitbucket.org/2.0/repositories/$project/$repository/pullrequests?state=merged" # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/pullrequests#get

$pullRequests = Invoke-RestMethod -Uri $uri
$pullRequests[0].values[0].description
$pullRequests[0].values[0].title
$pullRequests[0].values[0].created_on